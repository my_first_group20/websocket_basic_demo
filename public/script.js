//DOM elements
const sendBtn = document.getElementById("send-btn");
const displayMessages = document.getElementById("display-messages");
const messageInput = document.getElementById("message-input");

//create websocket client
const wsClient = new WebSocket("ws://localhost:3000");      //note: protocoll changed from http to ws!

wsClient.onopen = () => {
    console.log("Websocket connection opened")
}

//handle incoming message
wsClient.onmessage = async messageEvent => {
    const message = await messageEvent.data.text();
    showMessage(message)
}

//send message
sendBtn.onclick = () => {
    wsClient.send(messageInput.value);          //send to server
    showMessage(messageInput.value);            //display in own message box
    messageInput.value = "";                    //reset input value
}

//display
function showMessage (message) {
    displayMessages.textContent = displayMessages.textContent + `\n\n${message}`
    displayMessages.scrollTop = displayMessages.scrollHeight;
}

