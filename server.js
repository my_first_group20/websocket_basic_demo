const express = require("express");
const WebSocket = require("ws");
const http = require("http");

const port = 3000;

//Express server setup
const app = express();

//serve index.html from public folder
app.use(express.static("public"))

//http server setup using the previously created Express server
const httpServer = http.createServer(app);

//websocket setup using the previously created http server
const wsServer = new WebSocket.Server({server: httpServer})

//handle new connection, runs when browser client sends request to connect to server
wsServer.on("connection", (socket) => {                       //parameters: event, callback to handle event
    console.log("New socket connection");
    //handle message event, runs when one of the currently connected clients sends message
    socket.on("message", (data) => {
        //send incoming message to all currently connected clients, except for sender
        wsServer.clients.forEach(clientSocket => {
            if(clientSocket !== socket) {
                clientSocket.send(data)
            }
        })
    })

    //runs when client closes connection
    socket.on("close", (code, reason) => {
        console.log(`Socket connection closed: ${code} - ${reason}`);
    })
})

//start server
httpServer.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
})
