# Basic chat app for Websocket demo


## Usage

- clone repo
- run ```npm i```
- start server with ```npm run start```
- open two browsers, navigate to ```http://localhost:3000/```
- enter a message in one browser, the message shows up in the display field of the other browser

## Description

- server-side websocket code can be found in ```server.js``` file
- ```index.html``` file is served from public folder
- client-side websocket code can be found in ```script.js``` in public folder

